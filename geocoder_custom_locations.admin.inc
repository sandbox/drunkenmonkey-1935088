<?php

/**
 * @file
 * Administration form callbacks for the Geocoder custom locations module.
 */

/**
 * Page callback for the admin overview, showing a list of locations and the autocomplete options.
 */
function geocoder_custom_locations_admin_overview() {
  $theme['location_list'] = drupal_get_form('geocoder_custom_locations_settings_form');
  $theme['pager']['#theme'] = 'pager';
  $theme['handlers'] = drupal_get_form('geocoder_custom_locations_handlers_form');
  $theme['autocomplete'] = drupal_get_form('geocoder_custom_locations_autocomplete_form');

  return $theme;
}

/**
 * Form for letting admins view, edit and delete custom locations.
 *
 * @see geocoder_custom_locations_settings_form_validate()
 * @see geocoder_custom_locations_settings_form_submit()
 *
 * @ingroup forms
 */
function geocoder_custom_locations_settings_form(array $form, array &$form_state) {
  $header = array(
    'id' => array('data' => t('ID'), 'field' => 'id', 'sort' => 'asc'),
    'name' => array('data' => t('Name'), 'field' => 'name'),
    'location' => array('data' => t('Coordinates'), 'field' => 'location'),
  );

  // Fetch 50 locations (paged).
  $locations = db_select('geocoder_custom_locations', 'l')
    ->extend('PagerDefault')
    ->extend('TableSort')
    ->fields('l')
    ->limit(50)
    ->orderByHeader($header)
    ->execute();

  // Display the locations in a table with checkboxes.
  $options = array();
  foreach ($locations as $location) {
    $options[$location->id] = (array) $location;
  }
  $form['locations'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No custom locations defined.'),
  );

  // Add submit buttons only when displaying some values.
  if ($options) {
    $form['actions']['#type'] = 'actions';
    $form['actions']['edit'] = array(
      '#type' => 'submit',
      '#value' => t('Edit'),
    );
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }

  return $form;
}

/**
 * Form validate callback for geocoder_custom_locations_settings_form().
 *
 * @see geocoder_custom_locations_settings_form()
 * @see geocoder_custom_locations_settings_form_submit()
 */
function geocoder_custom_locations_settings_form_validate(array $form, array &$form_state) {
  $ids = array_filter($form_state['values']['locations']);
  if (!$ids) {
    form_set_error('locations', t('No location entries selected.'));
  }
}

/**
 * Form submit callback for geocoder_custom_locations_settings_form().
 *
 * @see geocoder_custom_locations_settings_form()
 * @see geocoder_custom_locations_settings_form_validate()
 */
function geocoder_custom_locations_settings_form_submit(array $form, array &$form_state) {
  $ids = array_filter($form_state['values']['locations']);

  switch ($form_state['values']['op']) {
    case t('Edit'):
      $ids = implode(',', $ids);
      $form_state['redirect'] = 'admin/config/content/geocoder/custom-locations/edit/' . $ids;
      break;
    case t('Delete'):
      $count = db_delete('geocoder_custom_locations')
        ->condition('id', $ids, 'IN')
        ->execute();
      $msg = format_plural($count,
        'Successfully deleted 1 custom location.',
        'Successfully deleted @count custom locations.');
      drupal_set_message($msg);
      break;
  }
}

/**
 * Form callback for the handlers settings form.
 *
 * @see geocoder_custom_locations_handlers_form_submit()
 *
 * @ingroup forms
 */
function geocoder_custom_locations_handlers_form(array $form, array &$form_state) {
  $enabled_handlers = variable_get('geocoder_custom_locations_handlers', array());
  $form['wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Geocoder handlers'),
    '#collapsible' => TRUE,
    '#collapsed' => !empty($enabled_handlers),
  );

  $handlers = array();
  $handler_info = geocoder_handler_info();
  foreach ($handler_info as $id => $handler) {
    if ($id != 'geocoder_custom_locations') {
      $handlers[$id] = check_plain($handler['title']);
    }
  }

  $form['wrapper']['handlers'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Add custom locations for these handlers'),
    '#options' => $handlers,
    '#default_value' => $enabled_handlers,
  );
  $form['wrapper']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save handlers'),
  );

  return $form;
}

/**
 * Form submit callback for geocoder_custom_locations_handlers_form().
 *
 * @see geocoder_custom_locations_handlers_form()
 */
function geocoder_custom_locations_handlers_form_submit(array $form, array &$form_state) {
  variable_set('geocoder_custom_locations_handlers', array_filter($form_state['values']['handlers']));
  drupal_set_message('The enabled Geocoder handlers were successfully saved.');
}

/**
 * Form callback for the autocomplete settings form.
 *
 * @see geocoder_custom_locations_autocomplete_form_validate()
 * @see geocoder_custom_locations_autocomplete_form_submit()
 *
 * @ingroup forms
 */
function geocoder_custom_locations_autocomplete_form(array $form, array &$form_state) {
  $form['autocomplete_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Autocomplete'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $autocomplete = variable_get('geocoder_custom_locations_autocomplete', array());
  $autocomplete_text = '';
  foreach ($autocomplete as $form_id => $fields) {
    foreach ($fields as $field) {
      $field = implode('|', $field);
      $autocomplete_text .= "$form_id $field\n";
    }
  }
  $form['autocomplete_wrapper']['autocomplete'] = array(
    '#type' => 'textarea',
    '#title' => t('Autocomplete fields'),
    '#description' => t('Enter forms and fields for which you want to activate autocompletion based on your custom locations. ' .
        'Each line should contain a form ID and the key of the text field (both as they appear in the Drupal form callback) for which autocompletion should be added, separated by one or more spaces. ' .
        'If the field is nested in a sub-form, specify the path to the field by separating all parent keys with "][" (e.g., "@views"). ' .
        'See the README.txt for details and examples. ' .
        'You can add autocompletion for several fields of the same form, but you have to use multiple lines for that.'),
    '#default_value' => $autocomplete_text,
  );
  $form['autocomplete_wrapper']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save autocomplete settings'),
  );

  return $form;
}

/**
 * Form validate callback for geocoder_custom_locations_autocomplete_form().
 *
 * @see geocoder_custom_locations_autocomplete_form()
 * @see geocoder_custom_locations_autocomplete_form_submit()
 */
function geocoder_custom_locations_autocomplete_form_validate(array $form, array &$form_state) {
  $lines = explode("\n", $form_state['values']['autocomplete']);
  $autocomplete = array();
  foreach ($lines as $line) {
    $line = trim($line);
    if ($line) {
      $line = array_filter(explode(' ', $line));
      if (count($line) != 2) {
        form_set_error('autocomplete_wrapper][autocomplete', t('Enter exactly one form ID and one field per line, separated by spaces.'));
        return;
      }
      list($form, $field) = array_merge($line);
      $autocomplete[$form][] = explode('|', $field);
    }
  }
  $form_state['values']['autocomplete'] = $autocomplete;
}

/**
 * Form submit callback for geocoder_custom_locations_autocomplete_form().
 *
 * @see geocoder_custom_locations_autocomplete_form()
 * @see geocoder_custom_locations_autocomplete_form_validate()
 */
function geocoder_custom_locations_autocomplete_form_submit(array $form, array &$form_state) {
  variable_set('geocoder_custom_locations_autocomplete', $form_state['values']['autocomplete']);
  drupal_set_message('The location autocomplete settings were successfully saved.');
}

/**
 * Form for letting admins define and edit custom locations.
 *
 * @param $ids
 *   (optional) A comma-separated list of IDs of locations to be edited. If
 *   empty, a form for adding new locations will be displayed.
 *
 * @see geocoder_custom_locations_edit_form_validate()
 * @see geocoder_custom_locations_edit_form_form_submit()
 *
 * @ingroup forms
 */
function geocoder_custom_locations_edit_form(array $form, array &$form_state, $ids = NULL) {
  if ($ids) {
    $ids = explode(',', $ids);
    $locations = geocoder_custom_locations_location_load_multiple($ids);
    if (!$locations) {
      drupal_access_denied();
    }
  }
  else {
    $locations = array_fill(0, 10, (object) array('id' => NULL, 'name' => '', 'location' => ''));
  }

  $form['#theme'] = 'geocoder_custom_locations_edit_table';
  $form['#tree'] = TRUE;

  $help = t('Specify the coordinates of locations as decimal latitude and longitude, separated by a comma. E.g., "40.67,-73.94" would be New York City.');
  $help .= '<br />';
  if ($ids) {
    $help .= t('You can clear the name field of locations which you want to remove from the list.');
  }
  else {
    $help .= t('Fill out as many lines as you like to add these locations and leave the rest blank.');
  }
  $form['description']['#markup'] = '<p>' . $help . '</p>';

  foreach ($locations as $i => $location) {
    $form['locations']["location-$i"]['id'] = array(
      '#type' => 'value',
      '#value' => $location->id,
    );
    $form['locations']["location-$i"]['id-text']['#markup'] = $location->id ? $location->id : t('New');
    $form['locations']["location-$i"]['name'] = array(
      '#type' => 'textfield',
      '#maxlength' => 100,
      '#default_value' => $location->name,
    );
    $form['locations']["location-$i"]['location'] = array(
      '#type' => 'textfield',
      '#maxlength' => 50,
      '#default_value' => $location->location,
    );
  }

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );
  $form['actions']['cancel']['#markup'] = l(t('Cancel'), 'admin/config/content/geocoder/custom-locations');

  return $form;
}

/**
 * Form validate callback for geocoder_custom_locations_edit_form().
 *
 * @see geocoder_custom_locations_edit_form()
 * @see geocoder_custom_locations_edit_form_submit()
 */
function geocoder_custom_locations_edit_form_validate(array $form, array &$form_state) {
  // Check whether all names have coordinates specified, too, and whether no
  // name was entered twice.
  foreach (element_children($form['locations']) as $key) {
    $row = &$form_state['values']['locations'][$key];
    $row['name'] = trim($row['name']);
    if (!empty($row['name'])) {
      if (isset($names[$row['name']])) {
        form_error($form['locations'][$key]['name'], t('You cannot specify the same name twice.'));
      }
      elseif ($row['name'] != $form['locations'][$key]['name']['#default_value']) {
        $names[$row['name']] = $key;
        $ids[$row['name']] = $row['id'];
      }
      // Be generous regarding whitespace and degree (°) symbols.
      $row['location'] = str_replace(array('°', ' ', "\t"), '', $row['location']);
      if (empty($row['location'])) {
        form_error($form['locations'][$key]['location'], t('You need to specify the coordinates of the location.'));
      }
      elseif (!preg_match("/^([+-]?[0-9]+(?:\.[0-9]+)?),([+-]?[0-9]+(?:\.[0-9]+)?)$/", $row['location'])) {
        form_error($form['locations'][$key]['location'], t('Please enter the coordinates as comma-separated decimal latitude and longitude.'));
      }
    }
  }

  // Check whether names already exist in the database.
  if (!empty($names)) {
    $sql = 'SELECT name, id FROM {geocoder_custom_locations} WHERE name IN (:names)';
    foreach (db_query($sql, array(':names' => array_keys($names)))->fetchAllKeyed() as $name => $id) {
      if ($id != $ids[$name]) {
        form_error($form['locations'][$names[$name]]['name'], t('A custom location with this name already exists.'));
      }
    }
  }
}

/**
 * Form submit callback for geocoder_custom_locations_edit_form().
 *
 * @see geocoder_custom_locations_edit_form()
 * @see geocoder_custom_locations_edit_form_validate()
 */
function geocoder_custom_locations_edit_form_submit(array $form, array &$form_state) {
  $inserts = $updates = 0;
  $deletes = array();
  $insert = db_insert('geocoder_custom_locations')
    ->fields(array('id', 'name', 'location'));
  foreach (element_children($form['locations']) as $key) {
    $row = &$form_state['values']['locations'][$key];
    if (empty($row['id'])) {
      if (!empty($row['name'])) {
        $row['id'] = NULL;
        $insert->values($row);
        ++$inserts;
      }
    }
    else {
      if (empty($row['name'])) {
        $deletes[] = $row['id'];
      }
      else {
        $updates += db_update('geocoder_custom_locations')
          ->fields(array(
            'name' => $row['name'],
            'location' => $row['location'],
          ))
          ->condition('id', $row['id'])
          ->execute();
      }
    }
  }

  if ($inserts + $updates == 0) {
    drupal_set_message(t('No locations entered.'), 'warning');
    return;
  }

  if ($inserts) {
    try {
      $insert->execute();
    }
    catch (PDOException $e) {
      drupal_set_message(t('An error occurred while saving the locations: @msg.', array('@msg' => $e->getMessage())), 'error');
      $form_state['rebuild'] = TRUE;
      return;
    }

    $msg = format_plural($inserts,
      'Successfully saved 1 new location.',
      'Successfully saved @count new locations.');
    drupal_set_message($msg);
  }

  if ($updates) {
    $msg = format_plural($updates,
      'Successfully updated 1 location.',
      'Successfully updated @count locations.');
    drupal_set_message($msg);
    $form_state['redirect'] = 'admin/config/content/geocoder/custom-locations';
  }

  if ($deletes) {
    $deletes = db_delete('geocoder_custom_locations')
      ->condition('id', $deletes, 'IN')
      ->execute();
    $msg = format_plural($deletes,
      'Successfully deleted 1 location.',
      'Successfully deleted @count locations.');
    drupal_set_message($msg);
    $form_state['redirect'] = 'admin/config/content/geocoder/custom-locations';
  }
}

/**
 * Themes a form as a row of editable locations.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: A render element representing the form.
 */
function theme_geocoder_custom_locations_edit_table(array $variables) {
  $form = $variables['element'];
  $header = array(t('ID'), t('Name'), t('Coordinates'));

  $rows = array();
  foreach (element_children($form['locations']) as $key) {
    $row = array();
    foreach (element_children($form['locations'][$key]) as $field) {
      if ($cell = render($form['locations'][$key][$field])) {
        $row[] = $cell;
      }
    }
    $rows[] = $row;
  }

  $actions = $form['actions'];
  unset($form['actions']);
  $output = drupal_render_children($form);
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= render($actions);

  return $output;
}
