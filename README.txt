Geocoder custom locations
=========================

This module allows you to specify custom locations for the Geocoder, which can
then be found without using external geocoding services. It also allows you to
add Javascript autocompletion to form elements which will use the list of
custom location as their source information.


  > How to: Set up the module

IMPORTANT: Until the Geocoder issue [1] is resolved, this module requires
manual patching of the Geocoder module to work. The patch is included in this
module as geocoder--caching_functionality.patch, or can be downloaded in the
Geocoder issue. It has to be applied to the latest development version of the
Geocoder module.
For information on how to apply patches, see [2].

[1] http://drupal.org/node/1515372
[2] http://drupal.org/patch/apply

Enable the module like normal. Then go to the module's configuration page at
  Administer > Configuration > Content authoring > Geocoder settings >
    > Custom locations
There you have, amongst others, the option for which Geocoder handlers you want
to add custom locations. For all handlers enabled here, the custom locations
will first be checked for matches when geocoding a value. Only when no matching
custom location is found, the handler is called like normal, otherwise the
custom location is returned.

Check all handlers for which you'd like this behaviour here, then click
"Save handlers".


  > How to: Adding custom locations

At the module's configuration page, you will also see a list of existing custom
locations (if any) and a link to "Add custom locations". Click there to add new
locations, or check the checkboxes next to existing locations to edit or delete
them (with the buttons on the bottom of the location list).

When adding locations, just fill in the lines of locations you want to create,
lines you leave empty will be ignored. The names of the locations have to be
unique, but can later be changed. For the coordinates, put in the latitude and
the longitude as decimal degrees (so, latitude between -90 (South pole) and 90
(North pole), longitude between -180 (180° east of Greenwich) and 180 (180° west
of Greenwich – both of the examples being equal, of course)), separated by a
single comma and no spaces. Use a decimal point for specifying the location more
exact.
You can usually find the coordinates of a location in its Wikipedia article (in
the top right). Click on the link to view the coordinates in decimal degrees
(instead of degrees, minutes, seconds).

Examples:
  New York City: 40.67,-73.94
  Paris: 48.86,2.35
  Tokyo: 35.69, 139.69


  > How to: Adding autocompletion to form fields

Due to technical restrictions, the specification of form fields to which
location-based autocompletion should be added is quite complicated. You have to
find out the form's internal form ID and the field's internal key (might be
nested inside of other keys). For the latter, the HTML "name" attribute of the
form field can usually be used, but this is not always the case.

When you have the form ID and the field key, enter them on a new line in the
autocomplete settings text area, separated by a space. If the field is nested
inside of subform arrays, concatenate all the keys leading to the field,
separating them with a vertical bar "|".

Examples:
- For a Views exposed filter which adds the parameter "location", use:
    views_exposed_form location
- For a Search API search page with a location filter on the
  "field_location:latlon" field, use:
    search_api_page_search_form form|field_location:latlon|field_location:latlon-location
- For the search block connected to the above-mentioned search page, use:
    search_api_page_search_form field_location:latlon|field_location:latlon-location


  > How to: Find out a form ID or a field key

This can be rather tricky. The probably easiest way to do this is as follows:

1) Install and enable the Devel module [3].

[3] http://drupal.org/project/devel

2) Create a new module [4] (or edit an existing one) and add a hook_form_alter()
  implementation. For example, if your module file is example.module, add this
  function at the bottom of the example.module file:

function example_form_alter(array &$form, array &$form_state, $form_id) {
  dpm($form_id);
  dpm($form);
}

[4] http://drupal.org/node/361112

3) Navigate to a page where the form in question is displayed. In the site's
  messages area, you'll get a list of all form IDs of the forms on this page, as
  well as all the form arrays (directly following the form ID to which they
  belong). Try to find the form you're looking for, note the form ID and then
  find your field in the corresponding form array (look, e.g., for the right
  label under "#title"). The sequence of links you had to click until the table
  with the right "#title" appeared is the key to the form field.


If all else fails, try to ask in the issue queue of the module which provides
the form (or the location field, if different).
